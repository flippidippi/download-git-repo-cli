# download-git-repo-cli
Download and extract a git repository (GitHub, Bitbucket) from node CLI.

> See [download-git-repo](https://gitlab.com/flippidippi/download-git-repo) for the API and issue tracker.

## Install

Require [Node.js](https://nodejs.org/en/) and [Git](https://git-scm.com/).

```bash
$ npm install download-git-repo-cli -g
```


## Usage

```bash
  Usage
    $ download-git-repo <url>
    $ download-git-repo <url> <directory>
    $ download-git-repo <url> <directory> --clone

  Example
    $ download-git-repo -h
    $ download-git-repo flippidippi/download-git-repo-fixture
    $ download-git-repo flippidippi/download-git-repo-fixture test/tmp
    $ download-git-repo gitlab:flippidippi/download-git-repo-fixture
    $ download-git-repo gitlab:custom.com:flippidippi/download-git-repo-fixture
    $ download-git-repo bitbucket:flippidippi/download-git-repo-fixture#my-branch test/tmp --clone

  Options
    -h, --help          Show help
    -c, --clone         Use git clone instead of an http download.
```


## License

MIT
